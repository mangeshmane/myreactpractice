import React from 'react';
import { shallow } from 'enzyme';
import Child from './Child';

describe('<Child />', () => {
  test('renders', () => {
    const wrapper = shallow(<Child />);
    expect(wrapper).toMatchSnapshot();
  });
});
