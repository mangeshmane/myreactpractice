import React, { Component } from 'react';

class Child extends Component {

  render() {
    console.log(this);
    return <div>
      <div className="badge badge-primary m-2">
        {this.props.name}<br />
        {this.props.hello}
      </div>
      <div>
        <button className="btn btn-primary m-2" onClick={this.props.greet}>Child</button>
       </div>
    </div>;
  }
}

export default Child;
